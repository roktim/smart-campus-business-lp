<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>大学生総合型マーケティングソリューション　スマートキャンパス</title>
    <meta name="keywords"content="タダコピ,タダch,canpass,学食トレー,大学内,学内ラック,学内ポスター,アプリ広告,サイト登録,会員登録,アプリダウンロード,レビュー,SNS拡散,学生モデル,キャスティング,工場移転,生協,食堂,書店,サンプリング,商品開発,学生向けEC,ミスコン,学祭">
    <meta name="description" content="スマートキャパスはタダコピのオーシャナイズが提供する大学生向けの総合型マーケティングソリューションです。大学生向けの企画立案や広告、集客など大学生に特化した様々なご提案をさせていただきます。">
<!--    Twitter bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">-->
<!--    custom css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-79136532-2', 'auto');
      ga('send', 'pageview');

    </script>
</head>
<link rel="icon" type="image/png" href="favicon.png">

<body id="topScroll">

<!-- /.load -->
    <div class="load">
        <div class="cssload-box-loading"></div>
    </div>
    <div class="container-fluid">
        <header class="row heading">
            <div class="container">
                <div class="col-md-2 logo">
                    <a href="#"><img src="assets/img/top_logo.png" alt="Logo"></a>
                </div>
                <!-- /.col-md-3 logo -->
                <div class="col-md-7 top_text">
                    <p><img src="assets/img/top_text_1.png" alt="Slogan"></p>
                    <p class="top_text_img_2" ><img src="assets/img/top_text_2.png" alt="Heading"></p>
                </div>
                <!-- /.col-md-6 top_text -->
                <div class="col-md-3 button">
        <!--            <a href="#"><img src="assets/img/top_btn.png" alt="Button"></a>-->
                    <a href="http://oceanize.co.jp/contact.php?referer=toB_LP" class="btn btn-danger btn-lg btn-red" onClick="ga('send', 'event', 'link', 'click', 'contact');"> お問い合わせはコチラから
                        <span class="icon"></span>
                     </a>
                </div>
                <!-- /.col-md-3 -->
                <!-- /.title -->
            </div>
            <!-- /.container -->
        </header>
        <!-- /.row -->
        <div class="row table_sec">
            <div class="container">
                <div class="table_heading">
                    <p class="table_heading_img_1" ><img src="assets/img/top_title.png" alt="Table Title"></p>
                    <p class="table_heading_img_2" ><img src="assets/img/sub_title.png" alt="Sub title"></p>
                </div>
                <!-- /.table_heading -->
                <div class="row tables">
                    <div class="col-md-4 p_table t_1">
                        <a href="#sec_1">
                        <div class="table_inner">

                            <div class="p_table_heading">
                                <p><img src="assets/img/table_1_title.png" alt="Table One Title"></p>
                            </div>
                            <!-- /.table_heading -->
                            <ul class="table_btns">
                                <li> <img src="assets/img/table_1_btn_1.png" alt="Button 1"></li>
                                <li> <img src="assets/img/table_1_btn_2.png" alt="Button 2"></li>
                                <li> <img src="assets/img/table_1_btn_3.png" alt="Button 3"></li>
                                <li> <img src="assets/img/table_1_btn_4.png" alt="Button 4"></li>
                                <li> <img src="assets/img/table_1_btn_5.png" alt="Button 5"></li>
                                <li> <img src="assets/img/table_1_btn_6.png" alt="Button 6"></li>
                                <li class="clearfix"><img src="assets/img/table_1_btn_7.png" alt="Button 7"></li>
                                <li><img src="assets/img/table_1_btn_8.png" alt="Button 8"></li>
                            </ul>

                        </div>
                        <!-- /.table_inner -->
                        </a>
                    </div>
                    <!-- /.col-md-4 table -->
                    <div class="col-md-4 p_table t_2">
                        <a href="#sec_3">
                        <div class="table_inner">
                            <div class="p_table_heading p_table_heading_2">
                                <p><img src="assets/img/table_2_title.png" alt="Table One Title"></p>
                            </div>
                            <!-- /.table_heading -->
                            <ul class="table_btns">
                                <li> <img src="assets/img/table_2_btn_1.png" alt="Button 1"></li>
                                <li> <img src="assets/img/table_2_btn_2.png" alt="Button 2"></li>
                                <li> <img src="assets/img/table_2_btn_3.png" alt="Button 3"></li>
                                <li> <img src="assets/img/table_2_btn_4.png" alt="Button 4"></li>
                                <li> <img src="assets/img/table_2_btn_5.png" alt="Button 5"></li>
                                <li> <img src="assets/img/table_2_btn_6.png" alt="Button 6"></li>
                                <li> <img src="assets/img/table_2_btn_7.png" alt="Button 7"></li>
                                <li> <img src="assets/img/table_2_btn_8.png" alt="Button 8"></li>
                            </ul>
                        </div>
                        <!-- /.table_inner -->
                        </a>
                    </div>
                    <!-- /.col-md-4 table -->
                    <div class="col-md-4 p_table t_3">
                        <a href="#sec_5">
                        <div class="table_inner">
                            <div class="p_table_heading">
                                <p><img src="assets/img/table_3_title.png" alt="Table One Title"></p>
                            </div>
                            <!-- /.table_heading -->
                            <ul class="table_btns">
                                <li><img src="assets/img/table_3_btn_1.png" alt="Button 1"></li>
                                <li><img src="assets/img/table_3_btn_2.png" alt="Button 2"></li>
                                <li><img src="assets/img/table_3_btn_3.png" alt="Button 3"></li>
                                <li><img src="assets/img/table_3_btn_4.png" alt="Button 4"></li>
                                <li><img src="assets/img/table_3_btn_5.png" alt="Button 5"></li>
                                <li><img src="assets/img/table_3_btn_6.png" alt="Button 6"></li>
                                <li><img src="assets/img/table_3_btn_7.png" alt="Button 7"></li>
                                <li><img src="assets/img/table_3_btn_8.png" alt="Button 8"></li>
                            </ul>
                        </div>
                        <!-- /.table_inner -->
                        </a>
                    </div>
                    <!-- /.col-md-4 table -->
                </div>
                <!-- /.row tables -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.row content -->
    </div>
    <!-- /.container -->

<div class="container-fluid">

        <div class="row medel_sec">

                <div class="medel_heading">
                   <div class="container"> <p class="text-center"><img src="assets/img/medel_title.png" alt="Medel Heading"></p></div>
                    <!-- /.container -->
                </div>
                <!-- /.medel_heading -->
            <div class="container">
                <div class="medel_body">
                    <ul class="medel_list">
                        <li><img src="assets/img/medel_1.png" alt="Medel One"></li>
                        <li><img src="assets/img/medel_2.png" alt="Medel Two"></li>
                        <li><img src="assets/img/medel_3.png" alt="Medel Three"></li>
                    </ul>
                </div>
                <!-- /.medel_body -->
            </div>
            <!-- /.container -->
            <div class="sec_footer">
                <p class="text-center">
    <!--                <a href="#"><img src="assets/img/medel_btn.png" alt="Medel Btn"></a>-->
                    <a href="http://oceanize.co.jp/contact.php?referer=toB_LP" class="btn btn-danger btn-lg btn-red" onClick="ga('send', 'event', 'link', 'click', 'contact');">お問い合わせはコチラから <span class="icon-large icon"></span></a>

                </p>
                <p class="text-center"><img class="contact" src="assets/img/contact_number.png" alt="Medel Conact"></p>
            </div>
            <!-- /.medel_footer -->
        </div>
        <!-- /.row medel_sec -->
</div>
<!-- /.row -->
<div class="container-fluid">
        <div class="parallax row">
        <div class="row sec_1" id="sec_1">
            <div class="container">
                <div class="sec_1_text">
                    <p class="text-center"><img src="assets/img/sec_1_text_1.png" alt="Text"></p>
                </div>
                <div class="sec_1_text">
                    <p class="text-center"><img src="assets/img/sec_1_text_3.png" alt="Text"></p>
                </div>
                <div class="sec_1_text">
                    <p class="text-center"><img src="assets/img/sec_1_text_2.png" alt="Text"></p>
                </div>
                <!-- /.sec_1_text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.row sec_1 -->

        <div class="row sec_2" id="sec_2">
                <div class="sec_title bg_dark">

                    <div class="title_text container">
                        <p class="text-center"><img src="assets/img/sec_2_title.png" alt="Section Two Title"></p>
                    </div>
                    <!-- /.title_text -->

                </div>
                <!-- /.sec_title -->
                <div class="sec_2_body">
                    <div class="container">
                        <div class="sec_heading">
                            <p class="text-center"><img src="assets/img/sec_2_text_1.png" alt="Text"></p>
                        </div>
                        <!-- /.sec_heading -->
                        <div class="sec_2_text">
                            <p class="text-center"><img src="assets/img/sec_2_text_2.jpg" alt="Text Two"></p>
                        </div>
                        <!-- /.sec_2_text -->
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.sec_2_body -->
                <!-- /.row sec_2 -->
                <div class="sec_footer">
                    <div class="container">
                        <p class="text-center"><a href="http://oceanize.co.jp/contact.php?referer=toB_LP" class="btn btn-danger btn-lg btn-red" onClick="ga('send', 'event', 'link', 'click', 'contact');">お問い合わせはコチラから <span class="icon-large icon"></span></a></p>
                        <p class="text-center"><img class="contact" src="assets/img/contact_number.png" alt="Section Conact"></p>
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.medel_footer -->

        </div>

    </div>
    <!-- /.parallax -->

</div>
<!-- /.container-fluid -->
<div class="container-fluid">
    <div class="parallax_2 row">
        <div class="container">
            <div class="row sec_3" id="sec_3">
                <div class="sec_1_text">
                    <p class="text-center"><img src="assets/img/sec_3_text_1.png" alt="Text"></p>
                </div>
                <div class="sec_1_text">
                    <p class="text-center"><img src="assets/img/sec_3_text_3.png" alt="Text"></p>
                </div>
                <div class="sec_1_text">
                    <p class="text-center"><img src="assets/img/sec_3_text_2.png" alt="Text"></p>
                </div>
                <!-- /.sec_1_text -->
            </div>
            <!-- /.row sec_1 -->
        </div>
        <!-- /.container -->
        <div class="row sec_4" id="sec_4">
            <div class="sec_title bg_dark">
                <div class="title_text container">
                    <p class="text-center"><img src="assets/img/sec_4_title.png" alt="Section Four Title"></p>
                </div>
                <!-- /.title_text -->

            </div>
            <div class="sec_2_body">
                <div class="container">
                    <div class="sec_heading">
                        <p class="text-center"><img src="assets/img/sec_4_text_1.png" alt="Text"></p>
                    </div>
                    <!-- /.sec_heading -->
                    <div class="sec_2_circle">
                        <ul>
                            <li><img src="assets/img/sec_4_circle_1.png" alt="Circle One"></li>
                            <li><img src="assets/img/sec_4_circle_2.png" alt="Circle Two"></li>
                            <li><img src="assets/img/sec_4_circle_3.png" alt="Circle Three"></li>
                            <li><img src="assets/img/sec_4_circle_4.png" alt="Circle Four"></li>
                            <li><img src="assets/img/sec_4_circle_5.png" alt="Circle Five"></li>
                            <li><img src="assets/img/sec_4_circle_6.png" alt="Circle Six"></li>
                        </ul>
                    </div>
                    <!-- /.sec_2_text -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.sec_2_body -->
            <!-- /.row sec_2 -->
            <div class="sec_footer">
                <div class="container">
                    <p class="text-center"><a href="http://oceanize.co.jp/contact.php?referer=toB_LP" class="btn btn-danger btn-lg btn-red" onClick="ga('send', 'event', 'link', 'click', 'contact');">お問い合わせはコチラから <span class="icon-large icon"></span></a></p>
                    <p class="text-center"><img class="contact" src="assets/img/contact_number.png" alt="Section Conact"></p>
                </div>
                <!-- /.container -->
            </div>
            <!-- /.medel_footer -->
        </div>
        <!-- /.row sec_4 -->
    </div>
    <!-- /.parallax_2 -->

</div>
<!-- /.container-fluid -->
<div class="container-fluid">

        <div class="parallax_3 row">
            <div class="row sec_5" id="sec_5">
                <div class="container">
                <div class="sec_1_text">
                    <p class="text-center"><img src="assets/img/sec_5_text_1.png" alt="Text"></p>
                </div>
                <div class="sec_1_text">
                    <p class="text-center"><img src="assets/img/sec_5_text_3.png" alt="Text"></p>
                </div>
                <div class="sec_1_text">
                    <p class="text-center"><img src="assets/img/sec_5_text_2.png" alt="Text"></p>
                </div>
                <!-- /.sec_5_text -->
                </div>
                <!-- /.container -->
            </div>
                <!-- /.parallax_3 -->
    </div>

</div>
<!-- /.container-fluid -->
<div class="container-fluid">
     <div class="sec_footer row final_footer">
         <div class="container">
            <div class="text-center footer_title"><img  src="assets/img/sec_6_text_1.png" alt="Section Conact"></div>
            <p class="text-center"><a href="http://oceanize.co.jp/contact.php?referer=toB_LP" class="btn btn-danger btn-lg btn-red" onClick="ga('send', 'event', 'link', 'click', 'contact');">お問い合わせはコチラから <span class="icon-large icon"></span></a></p>
            <p class="text-center"><img class="contact" src="assets/img/contact_number.png" alt="Section Conact"></p>
         </div>
         <!-- /.container -->
    </div>
</div>
<!-- /.container-fluid -->
<a href="#topScroll" class="scrollToTop"> TOPへ <img src="assets/img/arrow-up.png" alt="Arrow Top"></a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script>
    $(window).on('load resize', function(){
        var width = $(window).width(), height = $(window).height();
        if ( (width < 992)) {
            var deviceAgent = navigator.userAgent.toLowerCase();
            window.location.replace('sp_index.php');
        }
    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.5/TweenMax.min.js"></script>

<script src="http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
<script src="assets/js/animation.gsap.min.js"></script>
<script src="assets/js/plugins/ScrollToPlugin.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>

<script src="assets/js/scirpt.js"></script>

</body>
</html>
