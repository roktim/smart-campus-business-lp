jQuery(window).load(function(){
    $('html,body').animate({ scrollTop: 0 }, '1');
    TweenMax.set('.p_table',{opacity:0});

    setTimeout(function(){

        $(".load").fadeOut(500,function(){

            swing('.t_1');

            setTimeout(function(){
                swing('.t_2');
            },400);

            setTimeout(function(){
                swing('.t_3');
            },800);
        });

    },1000);
})


function swing(target) {
    // the values in vars can (and should) be tweaked to modify the way the swing works
    // * = affected by power
    var vars = {
        origin: 'center center',   // transformOrigin
        perspective: 800,       // transformPerspective
        ease: Power1.easeInOut, // an easeInOut should really be used here...
        power: 0.5,               // multiplier for the effect that is reduced to 0 over the duration
        duration: 0.3,            // total length of the effect (well, it can be up to vars.speed longer than this)
        rotation: -45,          // start rotation, also stores target rotations during tween
        maxrotation: 60,        // * max rotation after starting
        speed: 0.1,             // minimum duration for each swing
        maxspeed: 0.1           // * extra duration to add to the larger swings (any sort of real physics seems like overkill)
    };

    // target could be a string selector (it will be selected each swing though...), or a DOM or jQuery object
    vars.target = target;

    // starting position
    TweenMax.set(vars.target, {x:300, rotationY: vars.rotation, transformOrigin: vars.origin, transformPerspective: vars.perspective });


    TweenMax.to(vars, vars.duration, {opacity:1, power: 0, delay:.5, onStart: nextSwing, onStartParams: [vars] });


}


function nextSwing(vars) {
    if (vars.power > 0) {
        vars.rotation = (vars.rotation > 0 ? -1 : 1) * vars.maxrotation * vars.power;
        TweenMax.to(vars.target, vars.speed + vars.maxspeed * vars.power, {x:0, opacity:1, rotationY: vars.rotation, ease: vars.ease, onComplete: nextSwing, onCompleteParams: [vars] });
    } else {
        TweenMax.to(vars.target, vars.speed, {x:0, opacity:1, rotationY: 0, ease: vars.ease, clearProps: 'all' });
    }
}

$(function(){


    $(window).scroll(function(){
        if ($(this).scrollTop() > 80) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });


    var controller = new ScrollMagic.Controller();

    var sec_1_tl = new TimelineLite();
    var sec_2_tl = new TimelineLite();
    var sec_3_tl = new TimelineLite();

    var sec_1 = sec_1_tl.staggerFrom('.sec_3 .sec_text_1',.5,{opacity:0,y:100},0)
                        .staggerFrom('.sec_3 .sec_text_2',.5,{opacity:0,y:100},0.2)
                        .staggerFrom('.sec_3 .sec_text_3',.5,{opacity:0,y:100},0.2);

    var sec_2 = sec_2_tl.staggerFrom('.sec_5 .sec_text_1',.5,{opacity:0,y:100},0.5)
                        .staggerFrom('.sec_5 .sec_text_2',.5,{opacity:0,y:100},0.5)
                        .staggerFrom('.sec_5 .sec_text_3',.5,{opacity:0,y:100},0.5);

    var sec_3 = sec_3_tl.staggerFrom('.sec_7 .sec_text_1',.5,{opacity:0,y:100},0.4)
                        .staggerFrom('.sec_7 .sec_text_2',.5,{opacity:0,y:100},0.5)
                        .staggerFrom('.sec_7 .sec_text_3',.5,{opacity:0,y:100},0.5);

    //var sec_2 = sec_2_tl.staggerFrom('#parallex_2 .sec_1_text',.5,{opacity:0,y:150},0.4);
    //var sec_3 = sec_3_tl.staggerFrom('#parallex_3 .sec_1_text',.5,{opacity:0,y:150},0.4);
    //staggerFrom();

    var scene = new ScrollMagic.Scene(
        {
            triggerElement: ".sec_2",
            //duration: 400,
            triggerHook: "onLeave"
        })
        .setTween(sec_1)
        //.addIndicators() // add indicators (requires plugin)
        .addTo(controller);

    var scene = new ScrollMagic.Scene(
        {
            triggerElement: ".sec_4",
            //duration: 350,
            triggerHook:'onLeave'
        })
        .setTween(sec_2)
        //.addIndicators() // add indicators (requires plugin)
        .addTo(controller);

    var scene = new ScrollMagic.Scene(
        {
            triggerElement: ".sec_6",
            //duration: 400,
            triggerHook:'onLeave'
        })
        .setTween(sec_3)
        //.addIndicators() // add indicators (requires plugin)
        .addTo(controller);

    // change behaviour of controller to animate scroll instead of jump
    controller.scrollTo(function (newpos) {
        TweenMax.to(window, 1, {scrollTo: {y: newpos}});
    });

    $(document).on("click", "a[href^='#']", function (e) {
        var id = $(this).attr("href");
        if ($(id).length > 0) {
            e.preventDefault();

            // trigger scroll
            controller.scrollTo(id);

            // if supported by the browser we can even update the URL.
            if (window.history && window.history.pushState) {
                history.pushState("", document.title, id);
            }
        }
    });

})
