<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>大学生総合型マーケティングソリューション　スマートキャンパス</title>
    <meta name=”keywords” content=”タダコピ,タダch,canpass,学食トレー,大学内,学内ラック,学内ポスター,アプリ広告,サイト登録,会員登録,アプリダウンロード,レビュー,SNS拡散,学生モデル,キャスティング,工場移転,生協,食堂,書店,サンプリング,商品開発,学生向けEC,ミスコン,学祭”>
    <meta name=”description” content=”スマートキャパスはタダコピのオーシャナイズが提供する大学生向けの総合型マーケティングソリューションです。大学生向けの企画立案や広告、集客など大学生に特化した様々なご提案をさせていただきます。”>
    <!--    Twitter bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
    <!--    custom css -->
    <link rel="stylesheet" href="assets/css/style_sp.css">
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-79136532-2', 'auto');
      ga('send', 'pageview');

    </script>
</head>
<link rel="icon" type="image/png" href="favicon.png">

<body id="topScroll">
<div class="load">
    <div class="cssload-box-loading"></div>
</div>
<!-- /.load -->
<div class="container">
    <div class="row heading">
        <div class="col-md-3 col-sm-4 col-xs-4 logo">
            <a href="#"><img src="assets/img/sp/top_logo.png" alt="Site Logo"></a>
        </div>
        <!-- /.col-md-4 -->
        <div class="col-md-9 col-sm-4 col-xs-8 site_title">
            <p><img src="assets/img/sp/top_title.png" alt="Title"></p>
        </div>
        <!-- /.col-md-8 site_title -->
    </div>
    <!-- /.row heading-->
    <div class="sec_1 row">
        <div class="text-center sec_text_1 sec_title">
            <p><img class="img-responsive" src="assets/img/sp/sec_1_title.png" alt="Section One title"></p>
        </div>
        <div class="text-center sec_text_2">
            <p><img class="img-responsive" src="assets/img/sp/sec_1_slogan.png" alt="Slogan "> </p>
        </div>
        <div class="sec_btn"> <p class="text-center"><a href="http://oceanize.co.jp/contact.php?referer=toB_LP" class="btn btn-danger btn-lg btn-red" onClick="ga('send', 'event', 'link', 'click', 'contact');" >お問い合わせはコチラから <img class="icon" src="assets/img/btn_arrow.png" alt="button arrow"></a></p></div>
        <!-- /.sec_btn -->
        <div class="table_area">
            <div class="table p_table t_1">
                <a href="#parallex_1">
                    <div class="table_title"> <p class="text-center"> <img class="img-responsive" src="assets/img/sp/table_1_title.png" alt="Table Title"></p></div>
                    <!-- /.table_title -->
                    <ul>
                        <li> <img src="assets/img/sp/table_1_1.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_1_2.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_1_3.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_1_4.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_1_5.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_1_6.png" alt="Table element"></li>
                        <li class="big_btn"><img src="assets/img/sp/table_1_7.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_1_8.png" alt="Table element"></li>
                    </ul>
                </a>
            </div>
            <!-- /.table_1 -->
            <div class="table p_table t_2">
                <a href="#parallex_2">
                    <div class="table_title"> <p class="text-center"> <img class="img-responsive" src="assets/img/sp/table_2_title.png" alt="Table Title"></p></div>
                    <!-- /.table_title -->
                    <ul>
                        <li> <img src="assets/img/sp/table_2_1.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_2_2.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_2_3.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_2_4.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_2_5.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_2_6.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_2_7.png" alt="Table element"></li>
                        <li> <img src="assets/img/sp/table_2_8.png" alt="Table element"></li>
                    </ul>
                </a>
            </div>
            <!-- /.table_2 -->
            <div class="table p_table t_3">
                <a href="#parallex_3">
                    <div class="table_title"> <p class="text-center"> <img class="img-responsive" src="assets/img/sp/table_3_title.png" alt="Table Title"></p></div>
                    <!-- /.table_title -->
                    <ul>
                        <li><img src="assets/img/sp/table_3_1.png" alt="Table element"></li>
                        <li><img src="assets/img/sp/table_3_2.png" alt="Table element"></li>
                        <li><img src="assets/img/sp/table_3_3.png" alt="Table element"></li>
                        <li><img src="assets/img/sp/table_3_4.png" alt="Table element"></li>
                        <li><img src="assets/img/sp/table_3_5.png" alt="Table element"></li>
                        <li><img src="assets/img/sp/table_3_6.png" alt="Table element"></li>
                        <li><img src="assets/img/sp/table_3_7.png" alt="Table element"></li>
                        <li><img src="assets/img/sp/table_3_8.png" alt="Table element"></li>
                    </ul>
                </a>
            </div>

            <!-- /.table_3 -->
        </div>
        <!-- /.table_area -->
    </div>
    <!-- /.sec_1 -->
    <div class="row sec_2">
        <div class="sec_heading bg-blue">
            <p class="text-center"><img class="img-responsive" src="assets/img/sp/sec_2_title.png" alt="Section Title"></p>
        </div>
        <!-- /.sec_2_heading -->
        <div class="sec_body">
            <ul class="circles">
                <li><img src="assets/img/sp/circle_1.png" alt="Circle One"></li>
                <li><img src="assets/img/sp/circle_2.png" alt="Circle Two"></li>
                <li><img src="assets/img/sp/circle_3.png" alt="Circle Three"></li>
            </ul>
        </div>
        <!-- /.sec_body -->
        <div class="sec_footer bg-blue">
            <p class="text-center"><a href="http://oceanize.co.jp/contact.php?referer=toB_LP" class="btn btn-danger btn-lg btn-red" onClick="ga('send', 'event', 'link', 'click', 'contact');">お問い合わせはコチラから <img class="icon" src="assets/img/btn_arrow.png" alt="button arrow"></a></p>
            <p class="text-center"><img class="contact" src="assets/img/sp/sec_2_contact.png" alt=""></p>
        </div>
        <!-- /.sec_footer -->
    </div>
    <!-- /.sec_2 -->
    <div class="parallex_1 row" id="parallex_1">
        <div class="row sec_3">
            <div class="sec_text_1">
                <p class="text-center"><img src="assets/img/sp/sec_3_text_1.png" alt="Text one"> </p>
            </div>
            <!-- /.sec_text_2 -->
            <div class="sec_text_2">
                <p class="text-center"><img src="assets/img/sp/sec_3_text_2.png" alt="Text one"> </p>
            </div>
            <!-- /.sec_text_2 -->
            <div class="sec_text_3">
                <p class="text-center bottom_heading"><img src="assets/img/sp/sec_3_text_3.png" alt="Section Text"></p>
                <p class="text-center"><img src="assets/img/sp/sec_3_text_4.png" alt="Text one"> </p>
            </div>
            <!-- /.sec_text_2 -->
        </div>
        <!-- /.sec_3 -->
        <div class="sec_4 row">
            <div class="sec_heading bg-dark">
                <p class="text-center"><img src="assets/img/sp/sec_4_title.png" alt="Title"></p>
            </div>
            <!-- /.sec_heading bg-dark -->
            <div class="sec_body">
                <div class="sec_text_1">
                    <p class="text-center"><img class="img-responsive" src="assets/img/sp/sec_4_text_1.png" alt="Text"></p>
                </div>
                <!-- /.sec_text_1 -->
                <div class="sec_text_2">
                    <p class="text-center"><img class="img-responsive" src="assets/img/sp/sec_4_text_2.png" alt="Text"> </p>
                </div>
                <!-- /.sec_text_2 -->
            </div>
            <!-- /.sec_body -->
            <div class="sec_footer bg-blue">
                <p class="text-center"><a href="http://oceanize.co.jp/contact.php?referer=toB_LP" class="btn btn-danger btn-lg btn-red" onClick="ga('send', 'event', 'link', 'click', 'contact');">お問い合わせはコチラから <img class="icon" src="assets/img/btn_arrow.png" alt="button arrow"></a></p>
                <p class="text-center"><img class="contact" src="assets/img/sp/sec_4_contact.png" alt=""></p>
            </div>
            <!-- /.sec_footer -->
        </div>
        <!-- /.sec4 row -->
    </div>
    <!-- /.parallex_1 -->
    <div class="parallex_2 row" id="parallex_2">
        <div class="row sec_5">
            <div class="sec_text_1">
                <p class="text-center"><img src="assets/img/sp/sec_5_text_1.png" alt="Text one"> </p>
            </div>
            <!-- /.sec_text_2 -->
            <div class="sec_text_2">
                <p class="text-center"><img src="assets/img/sp/sec_5_text_2.png" alt="Text one"> </p>
            </div>
            <!-- /.sec_text_2 -->
            <div class="sec_text_3">
                 <p class="text-center"><img src="assets/img/sp/sec_5_text_3.png" alt="Text one"> </p>
            </div>
            <!-- /.sec_text_2 -->
        </div>
        <!-- /.sec_5 -->
        <div class="row sec_6">
            <div class="sec_heading bg-dark">
                <p class="text-center"><img src="assets/img/sp/sec_6_title.png" alt="Title"></p>
            </div>
            <!-- /.sec_heading bg-dark -->
            <div class="sec_body">
                <div class="sec_text_1">
                    <p class="text-center"><img class="img-responsive" src="assets/img/sp/sec_4_text_1.png" alt="Text"></p>
                </div>
                <!-- /.sec_text_1 -->
                <div class="sec_6_circles">
                    <ul>
                        <li><img src="assets/img/sp/sec_6_circle_1.png" alt="Circle"></li>
                        <li><img src="assets/img/sp/sec_6_circle_2.png" alt="Circle"></li>
                        <li><img src="assets/img/sp/sec_6_circle_3.png" alt="Circle"></li>
                        <li><img src="assets/img/sp/sec_6_circle_4.png" alt="Circle"></li>
                        <li><img src="assets/img/sp/sec_6_circle_5.png" alt="Circle"></li>
                        <li><img src="assets/img/sp/sec_6_circle_6.png" alt="Circle"></li>
                    </ul>
                 </div>
                <!-- /.sec_text_2 -->
                <div class="sec_footer bg-blue">
                    <p class="text-center"><a href="http://oceanize.co.jp/contact.php?referer=toB_LP" class="btn btn-danger btn-lg btn-red" onClick="ga('send', 'event', 'link', 'click', 'contact');">お問い合わせはコチラから <img class="icon" src="assets/img/btn_arrow.png" alt="button arrow"></a></p>
                    <p class="text-center"><img class="contact" src="assets/img/sp/sec_6_contact.png" alt=""></p>
                </div>
                <!-- /.sec_footer -->
            </div>
            <!-- /.sec_body -->
        </div>
        <!-- /.row sec_6 -->
    </div>
    <!-- /.parallex_2 -->
    <div class="parallex_3 row" id="parallex_3">
     <div class="row sec_7">
        <div class="sec_text_1">
            <p class="text-center"><img src="assets/img/sp/sec_7_text_1.png" alt="Text one"> </p>
        </div>
        <!-- /.sec_text_2 -->
        <div class="sec_text_2">
            <p class="text-center"><img src="assets/img/sp/sec_7_text_2.png" alt="Text one"> </p>
        </div>
        <!-- /.sec_text_2 -->
        <div class="sec_text_3">
            <p class="text-center"><img src="assets/img/sp/sec_7_text_3.png" alt="Text one"> </p>
        </div>
        <!-- /.sec_text_2 -->

        <div class="sec_footer bg-blue">
            <p class="text-center sec_7_footer_text"><img class="img-responsive" src="assets/img/sp/sec_7_footer_text.png" alt="Footer text"></p>
            <p class="text-center"><a href="http://oceanize.co.jp/contact.php?referer=toB_LP" class="btn btn-danger btn-lg btn-red" onClick="ga('send', 'event', 'link', 'click', 'contact');">お問い合わせはコチラから <img class="icon" src="assets/img/btn_arrow.png" alt="button arrow"></a></p>
            <p class="text-center"><img class="contact" src="assets/img/sp/sec_6_contact.png" alt=""></p>
        </div>
        <!-- /.sec_footer -->
    </div>
    </div>
    <!-- /.parallex_3 -->
    <!-- /.sec_7 -->
</div>
<!-- /.container -->
<a href="#topScroll" class="scrollToTop"> Top <img src="assets/img/arrow-up.png" alt="Arrow Top"></a>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    $(window).on('load resize', function(){
        var width = $(window).width(), height = $(window).height();
        if ( (width > 992)) {
            var deviceAgent = navigator.userAgent.toLowerCase();
            window.location.replace('index.php');

        }
    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.5/TweenMax.min.js"></script>

<script src="http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
<script src="assets/js/animation.gsap.min.js"></script>
<script src="assets/js/plugins/ScrollToPlugin.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>
<script src="assets/js/script_sp.js"></script>

</body>
</html>
